angular.module("app").service("CovidService", CovidService);
CovidService.$inject = ["$http"];
function CovidService($http) {
    this.getAllDays = function () {
        return $http.get("/api/countries");
    };

    this.getGlobalDays = function () {
        return $http.get("/api/global-days");
    };
}
