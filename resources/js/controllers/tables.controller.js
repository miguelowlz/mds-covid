angular.module("app").controller("TablesCtrl", TablesCtrl);
TablesCtrl.$inject = ["$scope", "CovidService"];

function TablesCtrl($scope, CovidService) {
    $scope.data = [];
    $scope.dataDays = [];
    $scope.labels = [];
    $scope.loadData = () => {
        CovidService.getAllDays().then((response) => {
            $scope.data = response.data;
        });

        CovidService.getGlobalDays().then((response) => {
            $scope.dataForChart(response.data);
        });
    };

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname; //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };

    $scope.dataForChart = function (data) {
        const { cases, deaths, recovered } = data;
        $scope.labels = Object.keys(cases);
        for (let [key, value] of Object.entries(cases)) {
            const indexOfDate = $scope.labels.indexOf(key);
            if (indexOfDate > 0) {
                const dayBeforeCases = cases[$scope.labels[indexOfDate - 1]];
                const dayBeforeDeaths = deaths[$scope.labels[indexOfDate - 1]];
                const dayBeforeRecovered =
                    recovered[$scope.labels[indexOfDate - 1]];
                const item = {
                    date: key,
                    cases: cases[key] - dayBeforeCases,
                    deaths: deaths[key] - dayBeforeDeaths,
                    recovered: recovered[key] - dayBeforeRecovered,
                };
                $scope.dataDays.push(item);
            }
        }
    };
}
