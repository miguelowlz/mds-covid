angular.module("app").controller("ChartCtrl", ChartCtrl);
ChartCtrl.$inject = ["$scope", "CovidService"];

function ChartCtrl($scope, CovidService) {
    $scope.labels = {
        cases: [],
        deaths: [],
        recovered: [],
    };

    $scope.chart = {
        cases: [],
        deaths: [],
        recovered: [],
    };

    $scope.loadData = () => {
        CovidService.getGlobalDays().then((response) => {
            const { cases, deaths, recovered } = response.data;
            if (cases) {
                $scope.dataForChart("cases", cases);
            }
            if (deaths) {
                $scope.dataForChart("deaths", deaths);
            }
            if (recovered) {
                $scope.dataForChart("recovered", recovered);
            }
        });
    };

    $scope.dataForChart = function (chart, data) {
        $scope.labels[chart] = Object.keys(data);
        const firstData = [];
        for (let [key, value] of Object.entries(data)) {
            const indexOfDate = $scope.labels[chart].indexOf(key);
            if (indexOfDate > 0) {
                const dayBefore = data[$scope.labels[chart][indexOfDate - 1]];
                firstData.push(value - dayBefore);
            }
        }
        $scope.chart[chart].push(firstData);
        $scope.labels[chart].shift();
    };
}
