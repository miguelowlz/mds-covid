<!DOCTYPE html>
<html ng-app="app" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Covid Dashboard</title>

    <!-- Styles -->
    <!-- CSS only -->
    <link href="{{mix('/css/app.css')}}" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Prueba FullStack</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
    <div class="container">
        <div ng-controller="ChartCtrl" ng-init="loadData()">
            <h1 class="text-center">Global data</h1>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">New Case per Day</h5>
                            </div>
                            <canvas id="bar-1" class="chart chart-bar" chart-data="chart.cases" chart-labels="labels.cases">
                            </canvas>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Patient Recovered per Day</h5>
                            </div>
                            <canvas id="bar-2" class="chart chart-bar" chart-data="chart.recovered" chart-labels="labels.recovered">
                            </canvas>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Deaths per Day</h5>
                            </div>
                            <canvas id="bar-3" class="chart chart-bar" chart-data="chart.deaths" chart-labels="labels.deaths">
                            </canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div ng-controller="TablesCtrl" ng-init="loadData()">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">List by country</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Global data per day</button>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col" ng-click="sort('country')">Country
                                    <span ng-show="sortKey=='country'" class="glyphicon sort-icon" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th scope="col">Total cases</th>
                                <th scope="col">New cases</th>
                                <th scope="col">Total recovered</th>
                                <th scope="col">New recovered</th>
                                <th scope="col">Total deaths</th>
                                <th scope="col">Total test</th>
                                <th scope="col">new test</th>
                                <th scope="col">Update date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="items in data | itemsPerPage:10 | orderBy:sortKey:reverse">
                                <td>@{{items.country}}</td>
                                <td>@{{items.cases + items.todayCases}}</td>
                                <td>@{{items.todayCases}}</td>
                                <td>@{{items.recovered + items.todayRecovered}}</td>
                                <td>@{{items.todayRecovered}}</td>
                                <td>@{{items.deaths + items.todayDeaths}}</td>
                                <td>@{{items.tests}}</td>
                                <td>@{{items.tests}}</td>
                                <td>@{{items.updated | date: 'MMM dd, yyyy'}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <dir-pagination-controls max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">New Cases</th>
                                <th scope="col">Patient recovered</th>
                                <th scope="col">Deaths</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="item in dataDays">
                                <td>@{{item.date}}</td>
                                <td>@{{item.cases}}</td>
                                <td>@{{item.recovered}}</td>
                                <td>@{{item.deaths}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <hr>
            <h2 style="text-align: center;">Country data</h2>
            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="data-per-day-tab" data-bs-toggle="tab" data-bs-target="#data-per-day" type="button" role="tab" aria-controls="data-per-day" aria-selected="true">Country Data Per Day</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="data-country-tab" data-bs-toggle="tab" data-bs-target="#data-country" type="button" role="tab" aria-controls="data-country" aria-selected="false">Data</button>
                </li>

            </ul>
            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade show active" id="data-per-day" role="tabpanel" aria-labelledby="data-per-day-tab">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col" ng-click="sort('country')">Country
                                    <span ng-show="sortKey=='country'" class="glyphicon sort-icon" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                </th>
                                <th scope="col">Total cases</th>
                                <th scope="col">New cases</th>
                                <th scope="col">Total recovered</th>
                                <th scope="col">New recovered</th>
                                <th scope="col">Total deaths</th>
                                <th scope="col">Total test</th>
                                <th scope="col">new test</th>
                                <th scope="col">Update date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="items in data | itemsPerPage:10 | orderBy:sortKey:reverse">
                                <td>@{{items.country}}</td>
                                <td>@{{items.cases + items.todayCases}}</td>
                                <td>@{{items.todayCases}}</td>
                                <td>@{{items.recovered + items.todayRecovered}}</td>
                                <td>@{{items.todayRecovered}}</td>
                                <td>@{{items.deaths + items.todayDeaths}}</td>
                                <td>@{{items.tests}}</td>
                                <td>@{{items.tests}}</td>
                                <td>@{{items.updated | date: 'MMM dd, yyyy'}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <dir-pagination-controls max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls>
                </div>
                <div class="tab-pane fade show active" id="data-country" role="tabpanel" aria-labelledby="data-country-tab">
                    <p>test</p>
                </div>
            </div>
        </div>

    </div>
</body>
<script src="{{mix('/js/app.js')}}"></script>

</html>