<?php
/**
 * DashboardController
 * @brief:  Class DashboardController
 * @author Miguel Angel Velasco Castiblanco <<migue1126wlz@gmail.com>>
 * @version   1.0 1.0 [<li>2021-09-25</li>]
 */

namespace App\Http\Controllers;

use App\Services\CovidServices;

class DashboardController extends Controller
{
    private $covidService;

    public function __construct()
    {
        $this->covidService = new CovidServices();
    }

    /**
     * DashboardController::countries
     * @brief: This method calls the countries method which consumes the Rest service 
     * @author: Miguel Angel Velasco Castiblanco <<migue1126wlz@gmail.com>>
     * @version: 1.0 1.0 [<li>2021-09-25</li>]
     * @access: public
     */
    public function countries()
    {
        return $this->covidService->countries();
    }

    /**
     * DashboardController::globalDataDaily
     * @brief: This method calls the countries method which consumes the Rest service 
     * @author: Miguel Angel Velasco Castiblanco <<migue1126wlz@gmail.com>>
     * @version: 1.0 1.0 [<li>2021-09-25</li>]
     * @access: public
     */
    public function globalDataDaily()
    {
        return $this->covidService->globalDataDaily();
    }
}
