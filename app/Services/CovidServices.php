<?php
/**
 * CovidSevices
 * @brief:  Class which contains the methods that perform the consumption of Rest Services  
 * @author Miguel Angel Velasco Castiblanco <<migue1126wlz@gmail.com>>
 * @version   1.0 [<li>2021-08-27</li>]
 */

namespace App\Services;

use Illuminate\Support\Facades\Http;

class CovidServices
{
    private $url;
    /**
     * CovidSevices::__construct
     * @brief: Metodo para el constructor de la clase
     * @author: Miguel Angel Velasco Castiblanco <<migue1126wlz@gmail.com>>
     * @version: 1.0 [<li>2021-09-25</li>]
     * @access: public
     */
    public function __construct()
    {
        $this->url = config('services.covid.endpoint');
    }

    /**
     * CovidSevices::countries
     * @brief: Method to consume the Rest service of the countries
     * @author: Miguel Angel Velasco Castiblanco <<migue1126wlz@gmail.com>>
     * @version: 1.0 [<li>2021-09-25</li>]
     * @access: public
     */
    public function countries()
    {
        return Http::get($this->url . '/v3/covid-19/countries')->json();
    }

    /**
     * CovidSevices::globalDataDaily
     * @brief: Method that consumes the history of the last 7 days
     * @author: Miguel Angel Velasco Castiblanco <<migue1126wlz@gmail.com>>
     * @version: 1.0 [<li>2021-09-25</li>]
     * @access: public
     */
    public function globalDataDaily()
    {
        $query_strring = 'lastdays=7';
        return Http::get($this->url . '/v3/covid-19/historical/all?' . $query_strring)->json();
    }
}